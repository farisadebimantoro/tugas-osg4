import 'package:flutter/material.dart';

void main() => runApp(MyApp());

/// This Widget is the main application widget.
class MyApp extends StatelessWidget {
  static const String _title = 'Eudeka - OSG4';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: Scaffold(
        appBar: AppBar(
          title: Text(_title),
          centerTitle: true,
          ),
        body: MyStatelessWidget(),
      ),
    );
  }
}

/// This is the stateless widget that the main application instantiates.
class MyStatelessWidget extends StatelessWidget {
  MyStatelessWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
          physics: BouncingScrollPhysics(),
          padding: EdgeInsets.all(5.0),
          children: <Widget>[
            Center(
              child: Container(
                child: Card(
                  child: Column(
                    children: <Widget>[
                      Image.asset('images/acc_1.jpg'),
                      ListTile(
                        title: Text('RF Online', style: TextStyle(fontSize: 15),),
                        subtitle: Text('Accretia', style: TextStyle(fontSize: 15),),
                        trailing: Text('18:00', style: TextStyle(fontSize: 25),),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(15, 0, 15, 15),
                        child: Text(
                              'Lake Oeschinen lies at the foot of the Blüemlisalp in the Bernese '
                                  'Alps. Situated 1,578 meters above sea level, it is one of the '
                                  'larger Alpine Lakes. A gondola ride from Kandersteg, followed by a '
                                  'half-hour walk through pastures and pine forest, leads you to the '
                                  'lake, which warms to 20 degrees Celsius in the summer. Activities '
                                  'enjoyed here include rowing, and riding the summer toboggan run.',
                                  textAlign: TextAlign.justify,
                                  softWrap: true,
                            ),
                      )
                    ],
                  ),
                )
                ),
            ),
            Center(             
              child: Container(
                child: Card(
                  child: Column(
                    children: <Widget>[
                      Image.asset('images/belato_1.jpg'),
                      ListTile(
                        title: Text('RF Online', style: TextStyle(fontSize: 15),),
                        subtitle: Text('Belato', style: TextStyle(fontSize: 15),),
                        trailing: Text('18:01', style: TextStyle(fontSize: 25),),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(15, 0, 15, 15),
                        child: Text(
                              'Lake Oeschinen lies at the foot of the Blüemlisalp in the Bernese '
                                  'Alps. Situated 1,578 meters above sea level, it is one of the '
                                  'larger Alpine Lakes. A gondola ride from Kandersteg, followed by a '
                                  'half-hour walk through pastures and pine forest, leads you to the '
                                  'lake, which warms to 20 degrees Celsius in the summer. Activities '
                                  'enjoyed here include rowing, and riding the summer toboggan run.',
                                  textAlign: TextAlign.justify,
                                  softWrap: true,
                            ),
                      )
                    ],
                  ),
                )
                ),
            ),
            Center(
              child: Container(
                child: Card(
                  child: Column(
                    children: <Widget>[
                      Image.asset('images/cora_1.jpg'),
                      ListTile(
                        title: Text('RF Online', style: TextStyle(fontSize: 15),),
                        subtitle: Text('Cora', style: TextStyle(fontSize: 15),),
                        trailing: Text('18:02', style: TextStyle(fontSize: 25),),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(15, 0, 15, 15),
                        child: Text(
                              'Lake Oeschinen lies at the foot of the Blüemlisalp in the Bernese '
                                  'Alps. Situated 1,578 meters above sea level, it is one of the '
                                  'larger Alpine Lakes. A gondola ride from Kandersteg, followed by a '
                                  'half-hour walk through pastures and pine forest, leads you to the '
                                  'lake, which warms to 20 degrees Celsius in the summer. Activities '
                                  'enjoyed here include rowing, and riding the summer toboggan run.',
                                  textAlign: TextAlign.justify,
                                  softWrap: true,
                            ),
                      )
                    ],
                  ),
                )
                ),
            ),
            Center(
              child: Container(
                child: Card(
                  child: Column(
                    children: <Widget>[
                      Image.asset('images/acc_2.jpg'),
                      ListTile(
                        title: Text('RF Online', style: TextStyle(fontSize: 15),),
                        subtitle: Text('All Race', style: TextStyle(fontSize: 15),),
                        trailing: Text('18:04', style: TextStyle(fontSize: 25),),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(15, 0, 15, 15),
                        child: Text(
                              'Lake Oeschinen lies at the foot of the Blüemlisalp in the Bernese '
                                  'Alps. Situated 1,578 meters above sea level, it is one of the '
                                  'larger Alpine Lakes. A gondola ride from Kandersteg, followed by a '
                                  'half-hour walk through pastures and pine forest, leads you to the '
                                  'lake, which warms to 20 degrees Celsius in the summer. Activities '
                                  'enjoyed here include rowing, and riding the summer toboggan run.',
                                  textAlign: TextAlign.justify,
                                  softWrap: true,
                            ),
                      )
                    ],
                  ),
                )
                ),
            ),
            Center(
              child: Container(
                child: Card(
                  child: Column(
                    children: <Widget>[
                      Image.asset('images/acc_3.jpg'),
                      ListTile(
                        title: Text('RF Online', style: TextStyle(fontSize: 15),),
                        subtitle: Text('All Race', style: TextStyle(fontSize: 15),),
                        trailing: Text('18:05', style: TextStyle(fontSize: 25),),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(15, 0, 15, 15),
                        child: Text(
                              'Lake Oeschinen lies at the foot of the Blüemlisalp in the Bernese '
                                  'Alps. Situated 1,578 meters above sea level, it is one of the '
                                  'larger Alpine Lakes. A gondola ride from Kandersteg, followed by a '
                                  'half-hour walk through pastures and pine forest, leads you to the '
                                  'lake, which warms to 20 degrees Celsius in the summer. Activities '
                                  'enjoyed here include rowing, and riding the summer toboggan run.',
                                  textAlign: TextAlign.justify,
                                  softWrap: true,
                            ),
                      )
                    ],
                  ),
                )
                ),
            ),
  ],
);
  }
}